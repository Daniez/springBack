package com.newspaper.newspaper.service;

import java.util.List;
import com.newspaper.newspaper.model.User;

public interface UserService {
	/**
	 * method that save user or update user
	 * @param user
	 * @return Usuario Guardado
	 */
	User save(User user);
	/**
	 * methot that search all user
	 * @return list users 
	 */
	List<User> findAll();
	/**
	 * methot that delete user 
	 * @param id
	 */
	void deleteUser(Long id);

}
