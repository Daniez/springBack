package com.newspaper.newspaper.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.newspaper.newspaper.model.User;
import com.newspaper.newspaper.model.dao.UserRepository;
import com.newspaper.newspaper.service.UserService;
import com.newspaper.util.RestResponse;

@RestController
public class UserController {

	@Autowired
	protected UserService userService;

	protected ObjectMapper mapper;

	// Method To save User
	@RequestMapping(value = "/userSave", method = RequestMethod.POST)
	public RestResponse saveOrUpdate(@RequestBody String userJson)
			throws JsonParseException, JsonMappingException, IOException {

		// Map json to class User
		this.mapper = new ObjectMapper();
		User user = this.mapper.readValue(userJson, User.class);

		if (!this.validateUser(user)) {
			return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), "Los Valores Son Obligaotrios");
		}

		this.userService.save(user);
		return new RestResponse(HttpStatus.OK.value(), "Guardado Exitosamente");
	}

	// list All users
	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public List<User> getUser() {
		return this.userService.findAll();
	}

	// list All users
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	public void deleteUser(@RequestBody String userJson) throws Exception {
		this.mapper = new ObjectMapper();
		User user = this.mapper.readValue(userJson, User.class);

		if(user.getId() == null) {
			throw new Exception("el id esta vacio");
		}

		this.userService.deleteUser(user.getId());
		new RestResponse(HttpStatus.OK.value(), "Borrado Exitosamente");
		
	}

	// Method To validate User
	public boolean validateUser(User user) {
		boolean isValid = true;
		if (user.getDocument() == null) {
			isValid = false;
			return isValid;
		} else if (user.getName() == null || user.getName().isEmpty()) {
			isValid = false;
			return isValid;
		} else if (user.getSecondName() == null || user.getSecondName().isEmpty()) {
			isValid = false;
			return isValid;
		} else if (user.getEmail() == null || user.getEmail().isEmpty()) {
			isValid = false;
			return isValid;
		} else if (user.getPassword() == null || user.getPassword().isEmpty()) {
			isValid = false;
			return isValid;
		}

		return isValid;
	}
}
