package com.newspaper.newspaper.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@Access(AccessType.FIELD)
public class User extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1244621317023540425L;
	@Column(name = "document", nullable = false, length = 250)
	private Long document;
	@Column(name = "email", nullable = false, length = 250)
	private String email;
	@Column(name = "secondname", nullable = false, length = 250)
	private String secondName;
	@Column(name = "cellphone", nullable = false, length = 250)
	private String cellPhone;
	@Column(name = "password", nullable = false, length = 250)
	private String password;

	
	public Long getDocument() {
		return document;
	}

	public void setDocument(Long document) {
		this.document = document;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
