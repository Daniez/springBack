package com.newspaper.newspaper.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.newspaper.newspaper.model.User;

public interface  UserRepository extends JpaRepository<User, Long>{
	
	

}
