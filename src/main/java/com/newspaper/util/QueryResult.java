package com.newspaper.util;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class QueryResult {
	
	public int totalRecords;
	public List<Object> list;
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	public List<Object> getList() {
		return list;
	}
	public void setList(List<Object> list) {
		this.list = list;
	}
	
}
